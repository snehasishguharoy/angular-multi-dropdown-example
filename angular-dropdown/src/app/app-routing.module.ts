import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { CpserviceComponent } from './components/cpservice.component';

const routes:Routes=[
  { path: 'costprofitcalculation', component: CpserviceComponent }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
   
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
