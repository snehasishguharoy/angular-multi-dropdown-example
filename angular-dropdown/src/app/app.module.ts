import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DropdownserviceService} from './service/dropdownservice.service';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
​
import {FormsModule} from '@angular/forms';
import { CpserviceComponent } from './components/cpservice.component';
import { AppRoutingModule } from './/app-routing.module';
@NgModule({
  declarations: [
    AppComponent,
    CpserviceComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxDatatableModule
  ],
  providers: [DropdownserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
