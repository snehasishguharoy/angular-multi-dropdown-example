import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DropdownserviceService } from '../service/dropdownservice.service';
import { Year } from '../model/year.model';
import { Project} from '../model/project.model';
import { Revenue } from '../model/revenue.model';
import { Employee } from '../model/employee.model';
@Component({
  selector: 'app-cpservice',
  templateUrl: './cpservice.component.html',
  styleUrls: ['./cpservice.component.css']
})
export class CpserviceComponent implements OnInit {
  emps: Employee;
  years: Year[];
  projects: Project[];
  revenues: Revenue[];
  months=['january','february','march','april','may','june','july','august','september','october',
  'november','december'];
  yearParam: number;
  projectIdParam: number;
  monthParam: string;
  revenue: Revenue;
  text: number;
  count: number;
  constructor(private router: Router,private cpService: DropdownserviceService) { }

  ngOnInit() {
    this.cpService.getYears().subscribe(data=>{
      this.years=data;
    });
   
    this.cpService.getProjects().subscribe(data=>{
      this.projects=data;
    });
   
    this.cpService.getEmployee().subscribe(data=>{
      this.emps=data;
    })

  
  };


  onYearSelected(val:any){
    this.yearParam=val.target.value;
  }
  onProjectSelected(val:any){
    this.projectIdParam=val.target.value;
  }
  onMonthSelected(val:any){
    this.monthParam=val.target.value;
    this.cpCalculateService();
  }
cpCalculateService(): any{
  this.revenue=new Revenue(this.projectIdParam,this.monthParam,this.yearParam);
  console.log(this.revenue);
  this.cpService.calculateProjectRevenue(this.revenue).subscribe(data=>{
    this.revenues=data;
  });
  console.log(this.revenues);
}
}
