import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpserviceComponent } from './cpservice.component';

describe('CpserviceComponent', () => {
  let component: CpserviceComponent;
  let fixture: ComponentFixture<CpserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
