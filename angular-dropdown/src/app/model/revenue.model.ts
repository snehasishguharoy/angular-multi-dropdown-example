export class Revenue{
    projectId: number;
    month: string;
    year: number;
    revenue: number;
    projectName: string;
    billinghours: number;

    constructor(projectId: number,month:string,year:number){
        this.projectId=projectId;
        this.month=month;
        this.year=year;
    }
    

}