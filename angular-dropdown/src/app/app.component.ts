import { Component } from '@angular/core';
import { Employee } from './model/employee.model';
import { DropdownserviceService } from './service/dropdownservice.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  emps: Employee;
  emps1: Employee [] ;
  columns = [
    { prop: 'name' },
    { name: 'address' },
    { name: 'salary' }
  ];
  constructor(private router: Router,private cpService: DropdownserviceService) { }
  ngOnInit() {
    
    this.cpService.getEmployee().subscribe(data=>{
      this.emps=data;
    });
    this.cpService.getEmployees().subscribe(data=>{
      this.emps1=data;
    });
  
  };


}
