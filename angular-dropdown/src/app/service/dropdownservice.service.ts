import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Year } from '../model/year.model';
import { Project } from '../model/project.model';
import { Revenue } from '../model/revenue.model';
import { Employee } from '../model/employee.model';

const httpOptions={
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable()
export class DropdownserviceService {

  constructor(private http:HttpClient) { }
  baseUrlProjects: string='http://localhost:5555/dropdown/projects'
  baseUrlYear: string='http://localhost:5555/dropdown/years';
  baseUrlRevenue: string='http://localhost:5555/dropdown/revenue/calculate';
  employeeUrl: string='http://localhost:5555/dropdown/employee';
  employeeUrl1:string='http://localhost:5555/dropdown/employees';
  getYears(){
    return this.http.get<Year[]>(this.baseUrlYear);
  }
  getProjects(){
    return this.http.get<Project[]>(this.baseUrlProjects);
  }

  calculateProjectRevenue(revenue){
    return this.http.post<Revenue[]>(this.baseUrlRevenue,revenue);

  }

  getEmployee(){
    return this.http.get<Employee>(this.employeeUrl);
  }

  getEmployees(){
    return this.http.get<Employee[]>(this.employeeUrl1);
  }

}
