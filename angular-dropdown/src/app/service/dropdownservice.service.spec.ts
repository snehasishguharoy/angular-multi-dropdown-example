import { TestBed, inject } from '@angular/core/testing';

import { DropdownserviceService } from './dropdownservice.service';

describe('DropdownserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DropdownserviceService]
    });
  });

  it('should be created', inject([DropdownserviceService], (service: DropdownserviceService) => {
    expect(service).toBeTruthy();
  }));
});
